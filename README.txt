﻿This is a load balanced Web API with a SQL backend and an in memory dictionary.
Please refactor to use REST conventions
Please improve or remove logic that is unsafe/unsecure/unperformant/incorrect/. Use comments to describe the issues.
If you feel that there are further extensions that can be made, please add supportive comments.
Please review all lines of code.

What have we Done ?

1-Apply fluent validations
2- Apply Asynchrnous programming
3- Implement inmemory repository
4- use Exceptions
5- Create HealthCheck Endpoint
6-Create Integration Tests Project utilizing inmemory test server
