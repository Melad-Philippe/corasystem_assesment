﻿using CoraWebAPITest;
using CoraWebAPITest.DTO;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace CoraWebAPI.Tests
{
    [TestClass]
    public class ProjectTests
    {
        public ProjectTests() { }

        #region Shared instances
        private static WebApplicationFactory<Startup> _factory;
        private static HttpClient _client;
        #endregion

        #region Setup and Teardown
        [AssemblyInitialize]
        public static void AssemblyInitialize(TestContext context)
        {
            _factory = new WebApplicationFactory<Startup>();
            _client = _factory.CreateDefaultClient();
        }

        [AssemblyCleanup]
        public static void AssemblyCleanup()
        {
            _factory.Dispose();
            _client.Dispose();
        }
        #endregion

        #region HealthCheck test
        [TestMethod]
        public async Task HealthCheck_ReturnsOk()
        {
            var response = await _client.GetAsync("/healthCheck");

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
        #endregion

        #region Get All Project test
        [TestMethod]
        public async Task GetAllProjects_ReturnsSuccessAndExpectedData()
        {
            var response = await _client.GetAsync("/api/Project/");

            response.EnsureSuccessStatusCode();
            Assert.IsNotNull(response.Content);
            Assert.IsTrue(response.Content.Headers.ContentLength > 0);
            Assert.AreEqual("application/json", response.Content.Headers.ContentType.MediaType);
        }
        #endregion

        #region Delete Project Tests
        [TestMethod]
        public async Task DeleteValidProject_ReturnsAccepted()
        {
            var project = await _client.GetFromJsonAsync<IEnumerable<Project>>("/api/Project/");
            var res = await _client.DeleteAsync(string.Format("/api/Project/{0}", project.FirstOrDefault()?.Name));
            Assert.AreEqual(res.StatusCode, HttpStatusCode.NoContent);
        }
        [TestMethod]
        public async Task DeleteInValidProject_ReturnsNotFound()
        {
            var res = await _client.DeleteAsync(string.Format("/api/Project/{0}", "NotFound"));
            Assert.AreEqual(res.StatusCode, HttpStatusCode.NotFound);
        }
        #endregion

        #region Get Project Tests
        [TestMethod]
        public async Task GetValidProject_ReturnsOk()
        {
            var project = await _client.GetFromJsonAsync<IEnumerable<Project>>("/api/Project/");
            var res = await _client.GetAsync(string.Format("/api/Project/{0}", project.FirstOrDefault()?.Name));
            Assert.AreEqual(res.StatusCode, HttpStatusCode.OK);
        }
        [TestMethod]
        public async Task GetInValidProject_ReturnsNotFound()
        {
            var res = await _client.GetAsync(string.Format("/api/Project/{0}", "NotFound"));
            Assert.AreEqual(res.StatusCode, HttpStatusCode.NotFound);
        }
        #endregion

        #region Create Project tests
        [TestMethod]
        public async Task AddExistProject_ReturnsConflict()
        {
            var projects = await _client.GetFromJsonAsync<IEnumerable<Project>>("/api/Project/");
            var response = await _client.PostAsJsonAsync("/api/Project/", projects.FirstOrDefault());
            Assert.AreEqual(HttpStatusCode.Conflict, response.StatusCode);
        }

        [TestMethod]
        public async Task AddInvalidProject_ReturnsBadRequest()
        {
            var invalid = new Project()
            {
                Name = "TestProject",
                StartDate = System.DateTime.Now,
                Timezone = TimeZoneInfo.GetSystemTimeZones().FirstOrDefault().StandardName

            };
            var response = await _client.PostAsJsonAsync("/api/Project/", invalid);
            var content = await response.Content.ReadAsStringAsync();
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public async Task AddvalidProject_ReturnsCreatedAndAdded()
        {
            var invalid = new Project()
            {
                Name = "Melad Project",
                StartDate = System.DateTime.Now,
                Timezone = TimeZoneInfo.GetSystemTimeZones().FirstOrDefault().StandardName

            };
            var response = await _client.PostAsJsonAsync("/api/Project/", invalid);
            var content = await response.Content.ReadAsStringAsync();
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            var project = await _client.GetFromJsonAsync<Project>(string.Format("/api/Project/{0}", "Melad Project"));

            Assert.AreEqual(project.Name, "Melad Project");
        }
        #endregion

        #region Update project tests
        [TestMethod]
        public async Task UpdateWithInvalidProject_ReturnsBadRequest()
        {
            var invalid = new Project()
            {
                Name = "TestProject",
                StartDate = System.DateTime.Now,
                Timezone = TimeZoneInfo.GetSystemTimeZones().FirstOrDefault().StandardName

            };
            var response = await _client.PutAsJsonAsync("/api/Project/", invalid);
            var content = await response.Content.ReadAsStringAsync();
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }
        [TestMethod]
        public async Task UpdateNonExistProject_ReturnsBadRequest()
        {
            var NotFound = new Project()
            {
                Name = "TestProject",
                StartDate = System.DateTime.Now,
                Timezone = TimeZoneInfo.GetSystemTimeZones().FirstOrDefault().StandardName

            };
            var response = await _client.PutAsJsonAsync("/api/Project/", NotFound);
            var content = await response.Content.ReadAsStringAsync();
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }
        [TestMethod]
        public async Task UpdateValidProject_ReturnsAccepted()
        {
            var projects = await _client.GetFromJsonAsync<IEnumerable<Project>>("/api/Project/");

            var Existed = projects.FirstOrDefault();
            var toUpdate = new Project()
            {
                Name = Existed.Name,
                StartDate = System.DateTime.Now,
                Timezone = TimeZoneInfo.GetSystemTimeZones().FirstOrDefault().StandardName

            };
            var response = await _client.PutAsJsonAsync("/api/Project/", toUpdate);
            var content = await response.Content.ReadAsStringAsync();
            Assert.AreEqual(HttpStatusCode.Accepted, response.StatusCode);
            var project = await _client.GetFromJsonAsync<Project>(string.Format("/api/Project/{0}", toUpdate.Name));
            Assert.AreEqual(project.Timezone, toUpdate.Timezone);

        }
    } 
    #endregion
}