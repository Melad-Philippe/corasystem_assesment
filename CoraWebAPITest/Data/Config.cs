﻿
using CoraWebAPITest.DTO;
using System;
using System.Collections.Generic;

namespace CoraWebAPITest.Data
{
    public static class Config
    {

        public static IDictionary<int, Project>  GetProjects()
        {
            var project1 = new Project();
            project1.Name = "New IT System";
            project1.StartDate = DateTime.UtcNow;
            project1.Timezone = "Eastern Standard Time";

            var project2 = new Project();
            project2.Name = "Lease new hardware";
            project2.StartDate = DateTime.UtcNow;
            project2.Timezone = "Eastern Standard Time";

            var projects = new Dictionary<int, DTO.Project>
           {
                { 1, project1 }, { 2, project2 }
           };
            return projects;
        }
    }
}
