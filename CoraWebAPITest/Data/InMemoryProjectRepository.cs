﻿using CoraWebAPITest.DTO;
using CoraWebAPITest.Exceptions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoraWebAPITest.Data
{
    public class InMemoryProjectRepository : IProjectRepository
    {
        #region Fields
        private IDictionary<int, Project> _projects { get; set; }
        
        #endregion

        #region Constructors
        public InMemoryProjectRepository(IDictionary<int, Project> projects)
        {
            _projects = projects;
        }
        #endregion

        #region Methods Implementation
        public async Task<Project> AddNewProjectItem(Project project)
        {
            var res = await IsExist(project.Name);
              
            if(res == true)
                throw new DataExistException(string.Format("Project With name {0} already exists",project.Name));

            _projects.Add(_projects.Keys.Count + 1, project);

            return project;
        }

        public Task<bool> DeleteProjectItem(string name)
        {
            var pro = _projects.FirstOrDefault(p => p.Value.Name.Equals(name));

            if (pro.Value == null) return Task.FromResult(false);
            _projects.Remove(pro.Key);
            return Task.FromResult(true);
        }

        public Task<IEnumerable<Project>> GetAllProjectItems()
        {
            return Task.FromResult<IEnumerable<Project>>(_projects.Values.ToList());
        }

        public Task<Project> GetProjectItem(string name)
        {
            return Task.FromResult(_projects.Values.FirstOrDefault(p => p.Name.Equals(name)));
        }

        public Task<bool> UpdateProjectItem(Project project)
        {
            var projectToUpdate = _projects.FirstOrDefault(p => p.Value.Name.Equals(project.Name));
            if(projectToUpdate.Value == null)  return Task.FromResult(false);
            // Update the project
            _projects[projectToUpdate.Key] = project;
            return Task.FromResult(true);
        }
        #endregion

        private Task<bool> IsExist(string name)
        {
            var Exists = _projects.Values.Any(project => project.Name == name);
            return Task.FromResult(Exists);
        }
    }
}
