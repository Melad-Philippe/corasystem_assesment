﻿using CoraWebAPITest.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoraWebAPITest.Data
{
    public interface IProjectRepository
    {
        Task<IEnumerable<Project>> GetAllProjectItems();
        Task<Project> GetProjectItem(string name);
        Task<Project> AddNewProjectItem(Project project);
        Task<bool> UpdateProjectItem(Project project);
        Task<bool> DeleteProjectItem(string name);
    }
}
