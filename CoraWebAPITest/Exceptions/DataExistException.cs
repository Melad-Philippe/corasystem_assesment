﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoraWebAPITest.Exceptions
{
    public class DataExistException: Exception
    {
        public  string  Message { get; set; }
        public DataExistException(string message)
        {
            Message = message;
        }
    }
}
