﻿using System;

namespace CoraWebAPITest.DTO
{
    public class Project
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public string Timezone { get; set; }
    }
}
