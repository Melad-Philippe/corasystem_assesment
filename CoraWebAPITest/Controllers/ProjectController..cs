﻿using CoraWebAPITest.Data;
using CoraWebAPITest.DTO;
using CoraWebAPITest.Exceptions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoraWebAPITest.Controllers
{
    [ApiController]
    [Route("api/Project")]
    public class ProjectController : ControllerBase
    {
        #region Fields
        private IProjectRepository _repositoryProject { get; set; }
        private ITimezoneLookup _timezoneLookup { get; set; }
        #endregion

        #region Constructor
        public ProjectController(IProjectRepository repositoryProject)
        {
            _repositoryProject = repositoryProject;
        }
        #endregion

        #region Action Methods
        [HttpGet()]
        public async Task<ActionResult<IEnumerable<Project>>> GetAll()
        {
            var res = await _repositoryProject.GetAllProjectItems();
            return Ok(res);
        }

        [HttpGet("{name}")]
        public async Task<ActionResult<Project>> Get(string name)
        {
            if (string.IsNullOrWhiteSpace(name)) return NotFound();
            var pro = await _repositoryProject.GetProjectItem(name);
            if (pro == null) return NotFound();
            return Ok(pro);
        }

        [HttpPost()]
        public async Task<ActionResult<Project>> Add([FromBody] Project item)
        {
            try
            {
                var pro = await _repositoryProject.AddNewProjectItem(item);
                return CreatedAtAction(nameof(Get), new { name = item.Name }, item);
            }
            catch (DataExistException ex)
            {
                return Conflict(ex.Message);
            }
        }

        [HttpPut()]
        public async Task<ActionResult<Project>> Update([FromBody] Project item)
        {
            var isUpdated = await _repositoryProject.UpdateProjectItem(item);
            if (isUpdated == true) return AcceptedAtAction(nameof(Get), new { name = item.Name }, item);
            return BadRequest();
        }

        [HttpDelete("{name}")]
        public async Task<ActionResult<Project>> Delete(string name)
        {
            if (string.IsNullOrWhiteSpace(name)) return NotFound();

            var isdeleted = await _repositoryProject.DeleteProjectItem(name);
            if (isdeleted == true) return NoContent();
            return NotFound();
        }
        #endregion
        
        #region validation Methods
        // I used Fluent Validation Instead of these Methods
        // But also can be refactor as below
        #region Bad Old Code
        //         if (item != null)
        //            {
        //                if (item.Name.Length <= _dbColumnConstraint)
        //                {
        //                    if (!item.Name.StartsWith("Test"))
        //                    {
        //                        if (!item.Name.EndsWith("Test"))
        //                        {
        //                            if (!projects.Values.Any(project => project == item))
        //                            {
        //                                if (item.StartDate != null)
        //                                {
        //                                    return true;
        //                                }
        //                            }
        //                       }
        //                    }
        //                }
        //            } 
        #endregion
        private async Task<bool> IsValid(Project item)
        {
            //Avoid Branching , Return Immediately 

            var _dbColumnConstraint = 50;

            if (item == null)
                return false;

            if (item.Name.Length > _dbColumnConstraint || string.IsNullOrWhiteSpace(item.Name))
                return false;

            if (item.Name.StartsWith("Test"))
                return false;

            if (item.Name.EndsWith("Test"))
                return false;

            
            var values = await _repositoryProject.GetAllProjectItems();
            // Here we compare strings equality
            if (values.Any(project => project.Name == item.Name))
                return false;

            return true;
        }

        #region Old Code
        //var timezoneLookupResult = TimezoneLookup.GetTimezone(item.Timezone);

        //    if (timezoneLookupResult.isValid != true)
        //    {
        //        return false;
        //    }

        //    return true; 
        #endregion
        private bool IsValidTimezone(DTO.Project item)
        {
            // Easier to Read
            var timezoneLookupResult = _timezoneLookup.GetTimezone(item.Timezone);

            if (timezoneLookupResult.isValid == true)
                return true;

            return false;
        }
        #endregion
    }
}
