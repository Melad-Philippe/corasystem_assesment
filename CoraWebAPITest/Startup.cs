using CoraWebAPITest.Data;
using CoraWebAPITest.DTO;
using CoraWebAPITest.FluentValidations;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace CoraWebAPITest
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // To add health checks 
            services.AddHealthChecks();
            // Our Services
            services.AddSingleton(Config.GetProjects());
            services.AddSingleton<ITimezoneLookup>(new TimezoneLookup());
            services.AddScoped<IProjectRepository, InMemoryProjectRepository>();

            services.AddControllers()
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<ProjectValidator>()); // Add Fluent Validations with automatic Registeration
            //services.AddTransient<IValidator<Project>, ProjectValidator>(); // Register validator Individually

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "CoraWebAPITest", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CoraWebAPITest v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/healthCheck");
                endpoints.MapControllers();
            });
        }
    }
}
