﻿using CoraWebAPITest.DTO;
using FluentValidation;

namespace CoraWebAPITest.FluentValidations
{
    public class ProjectValidator : AbstractValidator<Project>
    {
        public ProjectValidator(ITimezoneLookup timezoneLookup)
        {
            // ValidationRules for Name Property
            RuleFor(project => project.Name).NotEmpty()
                                            .MaximumLength(80)
                                            .Custom((name, context) =>
                                            {
                                                if (name != null && name.StartsWith("Test"))
                                                    context.AddFailure("Project name should not start with test");

                                                if (name != null && !name.Equals("Test") && name.EndsWith("Test"))
                                                    context.AddFailure("Project name should not end with test");
                                            });

            // ValidationRules for StartDate Property
            RuleFor(project => project.StartDate).NotEmpty();

            // ValidationRules for Timezone Property
            RuleFor(project => project.Timezone).NotEmpty();
            RuleFor(project => project.Timezone).Custom((timezone, context) => {
                if (timezone != null)
                {
                    var timezoneLookupResult = timezoneLookup.GetTimezone(timezone);

                    if (timezoneLookupResult.isValid == false)
                        context.AddFailure("timezone is invalid");
                }
            });
        }
    }
}
